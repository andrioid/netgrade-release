var config = require("../config"); // Configuration file
var httpModule = require("http");
var urlModule = require("url");
var fsModule = require("fs");
var pathModule = require("path");
var mimeModule = require("mime"); // npm install mime
var rbytesModule = require("rbytes"); // npm install rbytes
var spawn = require("child_process").spawn;
var measurementStorageModel = require("./measurementStorageModel").model;

//var dnsModule = require('/usr/lib/node_modules/ndns/lib/ndns');
var dnsModule = require('ndns/lib/ndns'); // mdns packaging sillyness

var status = 1;  // (0: Offline, 1: Online, 2: Over Capacity)
var hostname = config.hostname;
var filePath = config.filePath;
var serverIP = config.serverIP;
var tokenList = {};

var downloadDataSize = 1024*1024; // 512K
var downloadData = null;

var clientDoneCallback = function(token) {
    var ipAddress = tokenList[token].remoteAddress;
    tools.ping(ipAddress, function(output) {
        tokenList[token]['dataList']['ping'] = output;
    });

}

var dnsCallback = function(name, address) {

    var res = box.parseHost(name);
    if (res) {
        if (!tokenList[res.token]) {
            console.error("Received DNS request on a token that does not exist ("+res.token+").");
            console.log(tokenList);
        } else {
            tokenList[res.token].fromDNScallback(res.sequence, new Date().getTime(), address);
        }
    }

}

var httpCallback = function(name) {
    var res = box.parseHost(name);
    if (res) {
        if (!tokenList[res.token]) { console.error("Token does not exist, no DNS request received yet. WTF?"); }
        tokenList[res.token].fromHTTPcallback(res.sequence, new Date().getTime());
/*
        for (key in tokenList) {
            console.log(tokenList[key]);
        }
*/
    }

}

var measurementCallback = function(rbuff, ipAddress) {
    if (downloadData == null) {
        downloadData = '';
        downloadData = '{\'';
        while(downloadData.length < downloadDataSize-4) {
            downloadData += 'M';
        }
        downloadData += '\'}';
    }

//    console.log("Measurement Callback: ("+rbuff+") ("+ipAddress+").");
//%    console.log("Created measurement object for "+rbuff);
    tokenList[rbuff] = new measurementStorageModel(rbuff);
    tokenList[rbuff].remoteAddress = ipAddress;
}

var getMeasureObject = function (token) {
    return tokenList[token];
}

var http = {
    routes: { // Make it so that routes can be (re)defined
        '/': function(request, response) {
            http.route('index.html', request, response);
        },
        '/getToken': function(request, response) {
            var rbuff = rbytesModule.randomBytes(16).toHex();
            measurementCallback(rbuff, request.socket.remoteAddress);
            response.writeHead('200', {"Content-Type":  'text/plain'});
            response.write(rbuff);
            response.end();

        },
        '/results': function(req, res) {
            var data = '';
            req.on('data', function(chunk) {
                data += chunk;
            });

            req.on('end', function() {
                var jsonObject;
                //    empty 200 OK response for now
                try {
                    jsonObject = JSON.parse(data);
                    if (jsonObject.token) {
                        var mObj = tokenList[jsonObject.token];
                        if (jsonObject.downloadData) {

                            for (var i in jsonObject.downloadData) {
                                // Adjusting the download values, because the random hostname is slowing it down
                                if (!tokenList[jsonObject.token].dataList['dns'][i]['dnsPerformance']) { console.error("No DNS data for this sequence, that's weird!"); }
                                var currentNameserverTime = tokenList[jsonObject.token].dataList['dns'][i]['dnsPerformance'];
                                var currentDownloadTime = jsonObject.downloadData[i]-currentNameserverTime;

                                if (!tokenList[jsonObject.token].dataList['download']) { tokenList[jsonObject.token].dataList['download'] = {}; }
                                if (!tokenList[jsonObject.token].dataList['downloadSpeed']) { tokenList[jsonObject.token].dataList['downloadSpeed'] = {}; }
                                tokenList[jsonObject.token].dataList['download'][i] = currentDownloadTime;
                                tokenList[jsonObject.token].dataList['downloadSpeed'][i] = downloadDataSize/(currentDownloadTime/1000); // ms to s
                                tokenList[jsonObject.token].downloadCounter = i;
                            }
                        }
                        if (jsonObject.uploadData) {
                            for (var i in jsonObject.uploadData) {
                                if (!tokenList[jsonObject.token].dataList['upload']) { tokenList[jsonObject.token].dataList['upload'] = {}; }
                                if (!tokenList[jsonObject.token].dataList['uploadSpeed']) { tokenList[jsonObject.token].dataList['uploadSpeed'] = {}; }
                                tokenList[jsonObject.token].dataList['upload'][i] = jsonObject.uploadData[i]
                                tokenList[jsonObject.token].dataList['uploadSpeed'][i] = downloadDataSize/(jsonObject.uploadData[i]/1000); // ms to s
                                tokenList[jsonObject.token].uploadCounter = i;
                            }
                        }
                        clientDoneCallback(jsonObject.token);
                    }
                }
                catch (e) {
                    console.log("Unable to parse JSON data when receiving results");
                    console.error(e);
                };
                res.writeHead(200, "OK", {'Content-Type': 'text/html'});
                res.end();
            });
        },
        '/status': function (req, res) {
            var status = {};
            var data = '';
            req.on('data', function(chunk) {
                data += chunk;
            });

            req.on('end', function() {
                var jsonObject;
                //    empty 200 OK response for now
                try {
                    jsonObject = JSON.parse(data);
                }
                catch (e) {
                    console.log("Unable to parse JSON data for status");
                };
                if (getMeasureObject(jsonObject.token)) {
                    res.writeHead(200, "OK", {'Content-Type': 'text/json'});
                    res.write(JSON.stringify({
                        statusCode: getMeasureObject(jsonObject.token).getStatus(),
                        data: tokenList[jsonObject.token].dataList
                    }));
                } else {
                    res.writeHead(500, "Internal Server Error", {'Content-Type': 'text/json'});
                    res.write("Error: No token.");
                }
                
                res.end();
            });
            
        },
        '/upload': function (req, res) {
                res.writeHead(200, "OK", {'Content-Type': 'text/html'});
                res.end();
        },
        '/download': function (req, res) {
                res.writeHead(200, "OK", {'Content-Type': 'application/json'});
                res.write(downloadData);
                res.end();
        },
        '/dnsPoke': function (req, res) {
                res.writeHead(200, "OK", {'Content-Type': 'application/json'});
                res.end();            
        }
    }
};
http.listen = function(port) {
    if (port == undefined) { port = 80; }
    httpModule.createServer(http.onRequest).listen(port);
    console.log("Listening for HTTP on port "+port+".");
    return 1;
}

http.onRequest = function(request, response) {
    var pathname = urlModule.parse(request.url).pathname;
    //console.log(request);
    console.log("Request for: "+pathname+" ("+request.headers.host+")");
    httpCallback(request.headers.host);
    http.route(pathname, request, response);
}

http.route = function(pathname, request, response) {
    if (typeof http.routes[pathname] === 'function') {
        http.routes[pathname](request, response);
    } else { // No handler for this path
        var realPath = pathModule.join(filePath, pathname);
        //console.log("Real path: "+realPath);

        box.fileExists(realPath,
            function() { // true
                box.readFile(realPath, function(err, data) {
                    if (err) { console.error(err); }
                    //var mimeType = "text/html"; // default
                    var mimeType = mimeModule.lookup(realPath);
                    response.writeHead(200, {"Content-Type":  mimeType});
                    response.write(data, "binary");
                    response.end();
                });
            },
            function() { // false
                response.writeHead(404, "text/html");
                response.write("File not found");
                response.end();
            }

        );
    }
}

var dns = {
    listen: function() {
        var server = dnsModule.createServer('udp4');
        server.bind(53);

        server.on("request", dns.responder);
    },
    responder: function(req, res) {
        //console.log(req);
        if (req.question.length != 1) { return; } // Just a dumb responder, if multiple request in one, we ignore
        switch (req.question[0].type) {
            case 1:
                //console.log("A request");
            break;

            case 2:
                console.log("NS request, not implemented");
            break;

            default:
                // do nothing
            break;
        }

        if (req.question.length == 1) { // No point in answering otherwise, this is just a dumb responder
            if (req.question[0].type != 1) { return; }
            var name = req.question[0].name;
            var nm = new RegExp("([a-z0-9]+)?(\.)?\.t\."+hostname+"$", "i");
            var nme = name.match(nm);
            //console.log("Type: "+req.question[0].type);
            if (!nme) {
                console.log("Refused DNS query, probably because your configuration file is wrong.");
                res.header.rcode = 5; // refuse
                res.send();


            } else {
                res.addRR(
                     dnsModule.ns_s.an,         // answer section
                     name,                 // name
//                req.question[0].type,
                     dnsModule.ns_t.a,          // type
                     dnsModule.ns_c.in,         // class
                     3600,                 // TTL
                     serverIP              // RDATA
                );
                res.header.qr = 1;          // query type answer
                res.header.aa = 1;         // authoritative answer
                res.header.ra = 0;         // recursion not available
                dnsCallback(name, req.rinfo.address); // Callback before responding
                res.send();
            }
        } else {
            console.log("DNS request longer than 1");
        }
    }
}

var box = {}; // Object for managing local box operations (interface to fs, among other things);
box.readFile = function(uri, cb) {
    fsModule.readFile(uri, "binary", cb);
}

box.fileExists = function(uri, truecb, falsecb) {
    pathModule.exists(uri, function(exists) {
       if (exists) { truecb(uri); } else { falsecb(uri); }
    });
}

box.parseHost = function(string) {
    var pattern = /([0-9]+)\.([a-z0-9]+)\.t\.([a-z0-9]+\.[a-z0-9]+\.[a-z0-9]+)(\:([0-9][0-9][0-9]?[0-9]?))?/i;
    var result;
    if (!string) { return false; }
    if (result = string.match(pattern)) {
        return {
            'sequence': result[1],
            'token': result[2],
            'baseHost': result[3],
            'port': result[5] // optional
        };
    } else {
        return false;
    }
}

var tools = {};

tools.ping = function(ipAddress, cb) {
    /*
        Todo: This shit is dangerous, we should sanitize the ip address
     */
    console.log("Ping called for "+ipAddress+".");
    var pingData = '';
    var pingError = '';
    var exec = spawn('ping', ['-c 10', ipAddress]);
    exec.stdout.on('data', function(data) {
        pingData += data;
    });
    exec.stderr.on('data', function(data) {
       pingError += data;
    });
    exec.on('exit', function(code) {
        var seqPattern = /:\ icmp_req=([0-9]+)\ ttl=([0-9]+)\ time=([0-9]+(\.[0-9]+)?)\ ms/i;
        var lossPattern = /([0-9]+)% packet loss/i;
        var statsPattern = /^rtt\ min\/avg\/max\/mdev\ =\ ([0-9\.]+)\/([0-9\.]+)\/([0-9\.]+)\/([0-9\.]+)/i;
        if (code == 0 || code == 1) {
            console.log("Ping finished with data");
            var pingDataSplit = pingData.split('\n');
            var pingDataFinal = {};
            for (var i in pingDataSplit) {
                var line = pingDataSplit[i];
                var lineMatch;
                if (line && line != '') {
                    if (lineMatch = line.match(seqPattern)) {
                        pingDataFinal[lineMatch[1]] = lineMatch[3];
                    } else if (lineMatch = line.match(lossPattern)) {
                        pingDataFinal['loss'] = lineMatch[1];
                    } else if (lineMatch = line.match(statsPattern)) {
                        pingDataFinal['min'] = lineMatch[1];
                        pingDataFinal['avg'] = lineMatch[2];
                        pingDataFinal['max'] = lineMatch[3];
                        pingDataFinal['mdev'] = lineMatch[4];
                    } else {
                        console.log("Unidentified ping line: "+ line)
                    }
                    //console.log("Ping Line: "+line);
                }
            }
            pingDataFinal['done'] = 1;
            cb(pingDataFinal);
        } else {
            cb({'Error': "Failed: "+pingError});
            console.error("Ping failed ("+code+") with an error: \n"+pingError);
        }
    });
    
}

var start = function() { // Init
    http.listen(8080);
    dns.listen();
}


exports.status = status;
exports.http = http;
exports.dns = dns;
exports.box = box;
exports.hostname = hostname;
exports.start = start;
