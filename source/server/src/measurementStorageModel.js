var numberOfExpectedMeasurements = 2; // this is nasty as hell, but i'm on a deadline

exports.model = function(token) {
    var t = this;
    this.token = token;
    this.status = 1; // ongoing
    this.remoteAddress = null;
    this.dataList = {
        download: {},
        upload: {},
        dns: {},
        ping: {}
    };
    this.downloadCounter = 0;
    this.uploadCounter = 0;
    this.results = {
        latency: undefined,
        pdv: undefined,
        packetloss: undefined,
        download_bitrate: undefined,
        upload_bitrate: undefined
    };

    /*
        Todo: Move these methods into a "MeasurementStorageController" - this is a data model
     */

    this.fromDNScallback = function(seq, ms, address) {
        console.log("from dns callback called");
        if (!t.dataList['dns'][seq]) {
            t.dataList['dns'][seq] = {};
        }
        t.dataList['dns'][seq].dnsRequest = ms;
        t.dataList['dns']['server'] = address;
    };

    this.fromHTTPcallback = function(seq, ms) {
        if (!t.dataList['dns'][seq]) { console.log("Sequence not registered, DNS should've already hit"); }
        t.dataList['dns'][seq].httpRequest = ms;
        t.dataList['dns'][seq].dnsPerformance = t.dataList['dns'][seq].httpRequest-t.dataList['dns'][seq].dnsRequest;
    };

    this.getStatus = function() {
        var sreturn = 1;
        if (t.downloadCounter == numberOfExpectedMeasurements) {
            sreturn = 2;
            if (t.uploadCounter == numberOfExpectedMeasurements) {
                sreturn = 3;
                if (t.dataList['ping'] && typeof(t.dataList['ping']) == 'object' && t.dataList['ping']['done'] && t.dataList['ping']['done'] == 1) {
                    sreturn = 10;
                }
            }
        }
        return sreturn;
    };

    this.setDownloadData = function(data) {
        t.dataList.download = data;
    };

    this.setUploadData = function(data) {
        t.dataList.upload = data;
    };

    this.setDNSData = function(seq, data) {
        t.dataList.dns[seq] = data;
    };
}