
var system = require('../src/system');
var http = system.http; // shorthand
var dns = system.dns;
var box = system.box;

function measurement (token) {
    var t = this; // So sub functions can access the mother object
    this.token = token;
    this.statusCode = 0; // (0: Initial, 1: Done, 2: Server Pending, 3: Client Pending)+
    this.tasks = {};
    this.tasks.client = [];
    this.tasks.server = [];
    
    this.getStatus = function() {
        return {
            'code': this.statusCode,
            'tasks': this.tasks
        };
    };
    this.setStatus = function (s) {
        this.statusCode = s;
    };
    this.addTask = function(taskName, taskExecutor) {
        var tasks = t.tasks.server;
        switch (taskExecutor) {
            case 'client':
                tasks = t.tasks.client;
            break;

            default:
                // Assume it's server
            break;
        }
        tasks.push(taskName);
    };
}

function task() {
    this.executor = 'server';
    this.results = {};
    this.name = "Unnamed Measurement Task";
}

function getToken() {
    if (system.status != 1) {
        return system.status;
    }
    
    return "token123";
}

exports.common = {
    should_return_token_on_request: function(test) {
        var token = getToken();
        test.equal("token123", token);
        test.done();
    },
    should_return_error_token_when_over_capacity: function(test) {
        system.status = 2;
        var token = getToken();
        test.equal(2, token);
        test.done();

    },
    should_return_measurement_status_initial: function(test) {
        var token = getToken();
        var mes = new measurement(token);
        var status = mes.getStatus();

        test.equal(0, status.code); // We expect initial status
        test.done();
    },
    should_return_measurement_status_done: function(test) {
        var mes = new measurement(getToken());
        mes.setStatus(1); // Indicating the measurements are finished

        test.equal(1, mes.getStatus().code);
        test.done();
    },
    should_return_measurement_status_pending_server: function(test) {
        var mes = new measurement(getToken());
        mes.setStatus(2); // Pending on Server

        test.equal(2, mes.getStatus().code);
        test.done();
    },
    should_return_measurement_status_pending_client: function(test) {
        var mes = new measurement(getToken());
        mes.setStatus(3); // Pending on Client

        var status = mes.getStatus();
        test.equal(status.code, 3);
        test.done();
    },
    should_return_measurement_status_pending_client_task: function(test) {
        var mes = new measurement(getToken());
        mes.setStatus(3); // Pending on Client
        mes.addTask('download', 'client');
        var status = mes.getStatus();
        //console.log(mes.tasks);
        test.equal(status.tasks.client.length ,1);
        test.done();
    },
    should_have_handler_for_webroot: function(test) {
        test.equal('function',typeof(http.routes['/']));
        test.done();
    },

    should_not_have_handler_for_pie: function(test) {
        test.notEqual('function',typeof(http.routes['/pie']));
        test.done();
    },

    should_return_httpok_header_for_webroot: function(test) {
        var HttpCode;
        var response = {}; // mock object for the response
        var request = {};
        response.writeHead = function(code, contentType) {
            HttpCode = code;
            test.equal(HttpCode, 200);
        }
        response.write = function(data) {
            // stub
        }
        response.end = function() { /* stub */ }
        http.routes['/'](request, response);

        test.done();
    },

    should_fallback_to_filesystem_if_no_route_found: function(test) {
        var HttpCode = null;
        var response = {}; // mock object for the response
        var request = {};
        box.fileExists = function(uri, truecb, falsecb) { // mocking the file exist to always return true
            truecb();
        };

        box.readFile = function(uri, cb) {
            cb(null, "pie");
        }

        response.writeHead = function(code, contentType) {
            HttpCode = code;
        }
        response.write = function(data) {
            test.equal("pie", data);
        }
        response.end = function() { /* stub */ }
        //http.routes['/'](request, response);
        http.route('doesExist.html', request, response);

        test.done();
    },

    should_start_web_server: function(test) {
        http.listen = function(port) { // Mock the listen function
            return 1;
        }
        var ret = http.listen(8080);
        test.equal(ret, 1);
        test.done();
    },

    should_start_dns_server: function(test) {
        var called = false;
        dns.listen = function() { // Mock the listen function
            called = true;
        }
        dns.listen();
        test.equal(true,called);
        test.done();
    },

    should_reply_on_dns_requests: function(test) {
        var ass = false;
        var request = {
            question: [
                {type: 1, name: "local.netgrade.me"}
            ]
        };
        var response = {
            send: function() { ass = true; },
            header: {}
        };
        dns.responder(request, response);

        test.equal(true, ass);
        test.done();
        
    },

    should_return_404_when_no_handler_and_no_file: function(test) {
        var HttpCode = null;
        var response = {}; // mock object for the response
        var request = {};
        box.fileExists = function(uri, truecb, falsecb) { // mocking the file exist to always return false
            falsecb();
        };

        response.writeHead = function(code, contentType) {
            HttpCode = code;
        }
        response.write = function(data) {
            // stub
        }
        response.end = function() { /* stub */ }
        //http.routes['/'](request, response);
        http.route('doesNotExist.html', request, response);
        test.equal(404, HttpCode);
        test.done();
    },

    should_receive_mime_for_request: function(test) {
        var mimeResponse;
        var response = {
            writeHead: function(code, contentType) {
                mimeResponse = contentType;
            },
            write: function(data) {
                // stub
            },
            end: function() {
                // stub
            }
        }; // mock object for the response
        var request = {};

        http.route('/', request, response);
        test.equal(mimeResponse, "text/html");
        test.done();
    },

    should_return_sequence_number_for_host: function(test) {
        var testString = "1.2a3b4c5d6d7e8f9g.t.de.netgrade.me:8082";
        var res = box.parseHost(testString);
        test.equal(res.sequence, "1");
        test.done();

    },

    should_return_token_for_host: function(test) {
        var testString = "1.2a3b4c5d6d7e8f9g.t.de.netgrade.me:8082";
        var res = box.parseHost(testString);
        test.equal(res.token, "2a3b4c5d6d7e8f9g");
        test.done();

    },

    should_return_baseHost_for_host: function(test) {
        var testString = "1.2a3b4c5d6d7e8f9g.t.de.netgrade.me:8082";
        var res = box.parseHost(testString);

        test.equal(res.baseHost,"de.netgrade.me");
      test.done();
    }
    /*

    should_return_port_for_host: function(test) {

    }
*/

/* (on hold)
    should_verify_results_of_client_task: function(test) {
        test.equal(1,0);
        test.done();
    }
*/
};
