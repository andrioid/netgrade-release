// Glue.js - DOM stuff, that doesn't need to be unit tested


// Configuration, should eventually be moved to the server side
var measureTotal = 2;
var measureCount = 0;

$(document).ready(function() {



    $('#measureNow').click(function(e) {
        e.preventDefault();
        client.getToken(function(token) {
            if (!typeof(token) == 'object') {
                alert("Unexpected response from server, try again later.");
                return;
            }

            if (token.type == 'token') {
                client.baseHost = window.location.host;
                $(document).trigger('measureStart', [token.data]);
            }
        });
    });
    $(document).bind('measureDone'), function(ev) {
        //
    }


    $(document).bind('measureError', function(event, param) {
        $('#measureLog').append('<strong>Error:</strong> '+param);
    });

    $('#measureResults').bind('processData', function(ev) {
        $('#measureResults #serversideStatus').html('<img src="src/checkmark.gif">');
        //$('#measureResults').append('<h3>Bazinga!</h3>');
        $('#measureLog').html(JSON.stringify(client.remoteData));
        console.log(client.remoteData);
    });


    $(document).bind('measureStart', function(event) {

        $('#measureResults').html(
            '<ul>'
            +'<li>Download: <span id="downloadStatus"></span></li>'
            +'<li>Upload: <span id="uploadStatus"></span></li>'
            +'<li>DNS: <span id="dnsStatus"></span></li>'
            +'<li>Server Measurements: <span id="serversideStatus"></span></li>'
            +'</ul>'
        );


        client.startTimer(function(eventCode) {
            if (eventCode == 10) {
                $('#measureLog').append('All done, according to server<br>');
                $('#measureResults').trigger('processData');
            }
        });

        var clientDownloadCallback = function(time) {
            measureCount++;
            var sec = time/1000;
            $('#measureLog').append('Downloaded 1M file in '+sec+' seconds.<br>');
            if (measureCount<measureTotal) {
                client.downloadFile(clientDownloadCallback);
            } else {
                measureCount = 0; // Reset
                $('#measureResults #downloadStatus').html('<img src="src/checkmark.gif">');
                $('#measureResults #uploadStatus').html('<img src="src/loader.gif">');
                client.uploadFile(clientUploadCallback);
            }
        }

        var clientUploadCallback = function(time) {
            measureCount++;
            var sec = time/1000;
            $('#measureLog').append('Uploaded 1M file in '+sec+' seconds.<br>');
            if (measureCount<measureTotal) {
                client.uploadFile(clientUploadCallback);
            } else {
                $('#measureResults #uploadStatus').html('<img src="src/checkmark.gif">');
                $('#measureResults #dnsStatus').html('<img src="src/loader.gif">');
                client.measureDNS(clientDNSCallback);
            }
        }

        var clientDNSTotal = 10;
        var clientDNSCount = 0;
        var clientDNSCallback = function() {
            if (clientDNSCount<clientDNSTotal) {
                client.measureDNS(clientDNSCallback);
                clientDNSCount++;
            } else {
                client.sendResults(function() {
                    $('#measureResults #dnsStatus').html('<img src="src/checkmark.gif">');
                    $('#measureResults #serversideStatus').html('<img src="src/loader.gif">');
                    // If we need to do something when done sending results
                });
            }

        }

        $('#measureResults #downloadStatus').html('<img src="src/loader.gif">');
        client.downloadFile(clientDownloadCallback);

    });
});