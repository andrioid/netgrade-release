var client = {  // Namespace
    seqNumber: 1,
    measureUrlCounter: 1, // Incremented by getMeasureUrl (unique benchmark urls)
    downloadCounter: 1,
    uploadCounter: 1,
    baseHost: null, // Default
    token: null,
    downloadData: {},
    uploadData: {}
};

client.getToken = function(cb) {
    $.ajax({
        type: 'get',
       url: '/getToken',
       dataType: 'text',
        success: function(data) {
            //console.log(typeof(data));
            client.token = data;
            cb({
                'type': 'token',
                'data': data
            });
            //console.log(data);
        }
    });
};

client.getMeasureUrl = function(req, cb) {
    cb("http://"+client.measureUrlCounter+"."+client.token+".t."+client.baseHost+req);
    client.measureUrlCounter++;
};

client.downloadFile = function(cb, filename) {
    var startTime, stopTime, resTime, url;
    if (filename == undefined) { filename = "/download"; }

/*
    client.getMeasureUrl(filename, function(murl) {
        url = murl;
        //console.log(url);
    });
*/
    url = "/download?token="+client.token+"&seq="+client.uploadCounter;

    $.ajax({
        url: url,
        cache: false,
        crossDomain: true,
        dataType: 'jsonp',
        processData: false,
        beforeSend: function (xhr, settings) {
            xhr.overrideMimeType( 'text/plain; charset=x-user-defined' );
            startTime = new Date().getTime();
        },
        complete: function(){
            stopTime = new Date().getTime();
            resTime = stopTime-startTime;
            client.downloadData[client.downloadCounter] = resTime;
            client.downloadCounter++;
            cb(resTime);
        }
        /*,
        error: function(jqXHR, textStatus, errorThrown) {
            $(document).trigger("measureError", ["An unexpected error occurred while fetching measurement data ("+url+").<br>"]);
        }
        */
    });
    //cb(120);
};

client.measureDNS = function(cb) {
    var url;
    var filename = "/dnsPoke";
    client.getMeasureUrl(filename, function(murl) {
        url = murl;
    });
    $.ajax({
        url: url,
        cache: false,
        crossDomain: true,
        dataType: 'jsonp',
        processData: false,
        complete: function(){
            cb();
        }
    });    
};

client.sendResults = function (cb) {
    //console.log("Sending results...");
    //console.log(client.uploadData);
    $.ajax({
        url: '/results',
        type: 'post',
        data: JSON.stringify({
            token: client.token,
            downloadData: client.downloadData,
            uploadData: client.uploadData
        }),
        success: function() {
            cb();
        }

    });
};

client.startTimer = function(cb) {
    $.ajax({
        url: '/status',
        type: 'post',
        data: JSON.stringify({
            token: client.token
        }),
        success: function(data) {
            var code = 1; // ongoing
            if (data.statusCode) {
                var code = data.statusCode;
                client.remoteData = data.data;
                //console.log("Status data.");
                //console.log(data);
                cb(code); // status code for UI
            }
            if (code != 10) { // If code isn't "done", then we set the timer again.
                client.timerId = setTimeout(function() {
                    client.startTimer(cb); // propogate the callback
                }, 5000); // 5 sec
            }
        }
    })
};

client.createData = function(size, cb) {
    //console.log("Creating data, size: "+size);
    var data = '';
    while (data.length < size) {
        data += "N";
    }
    cb(data);
}

client.uploadFile = function(cb, bytes) {
    if (bytes == undefined) { bytes = 1024*1024; }
    var startTime, stopTime, resTime, url;
    filename = "/upload?token="+client.token+"&seq="+client.uploadCounter;

    client.createData(bytes, function(data) {
        var seq = 1;
        //console.log("Got data, submitting it, size: "+data.length);
        $.ajax({
            url: '/upload',
            type: 'post',
            cache: false,
            data: data,
            beforeSend: function (xhr, settings) {
                startTime = new Date().getTime();
            },
            success: function(){
                stopTime = new Date().getTime();
                resTime = stopTime-startTime;
                client.uploadData[client.uploadCounter] = resTime;
                client.uploadCounter++;
                cb(resTime);
            }
        });
    });
}
