//var client = require('../src/client').client;
var client = {};

exports.common = {
  should_get_token_from_server: function(test) {
      client.getToken = function(cb) { // stub
          cb("token123");
      };
      client.getToken(function(token) {
        test.equal(token, "token123");
      });
      test.done();
  },
  should_return_download_time_for_file: function(test) {
      client.downloadFile = function(cb) {
          cb(323);
      }
      client.downloadFile(function(time) {
         test.equal(time, "323");
      });
      test.done();
  },
    should_return_upload_time_for_file: function(test) {
        test.equal(true, true);
        test.done();
    }
};